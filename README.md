vagrant-extfacts-demo
---------------------

Demonstrate external facts with Python, Ruby, and YAML.

# Usage
    $ git clone https://github.com/nrvale0/vagrant-extfacts-demo
    $ cd vagrant-extfacts-demo
    $ vagrant up
    $ vagrant ssh
    $ sudo su -
    # facter | egrep -i 'python|ruby|yaml'
    # ls /etc/facter/facts.d 
