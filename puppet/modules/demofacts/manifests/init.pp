class demofacts {

  package { 'facter': ensure => '1.7.4', provider => 'gem', }

  File { owner => 'root', group => 'root', mode => '0755', }

  file { '/etc/facter': ensure => directory, }

  file { '/etc/facter/facts.d':
    ensure => directory,
    recurse => true,
    source => "puppet:///modules/${module_name}",
  }
}
